# Merapar Interview Challenge

The purpose of this document is to provide technical details about the solution and give instruction to use. 
In the challenge the goal was to build a web server in a cloud environment which serves a dynamic page. 

## Cloud environment 

For the Cloud Environment i used terraform for provisioning with the following files:

main.tf: contains terraform initialization and provider for AWS  
network.tf: contains the network components like vpc, subnet, igw…  
security.tf: contains the security group for the instance  
resources.tf: create Ec2 instance and S3 bucket  
variables.tf: initialize all the variables used  
data.tf: datasource to pull the latest ami image  
output.tf: create outputs with Ip addresses  
dev.auto.tfvars: variable values to used automatically  

## Web server

A simple Apache web server is installed with PHP to support dynamic websites. The installation and the starting of the web server is automatic through user_data. An index.php file is also placed to /var/www/html by the deployment process. To change the dynamic string an S3 Bucket is created to store a file with the string variable. Changing the content of the file in the bucket will change the string.

## Deployment
### Prerequisites

To deploy the stack the following prerequisites are necessary:

AWS account  
Terraform  
AWS credentials are configured for default profile  

### Deployment

Clone the repo:
``` 
git clone https://gitlab.com/vjuhasz/merapar-challenge.git
```
Issue the following commands:
```
cd to merapar-challenge
terraform init 
terraform plan 
terraform apply -auto-approve
```

Wait a few minutes until terraform finishes and returns the Elastic IP address to connect. In a web browser visit the address http://<ip-address>. If the website is not accessible please wait a bit and refresh the page later, until the resources are getting created and the web server is installed. 

To change the string value please use aws cli or the console to amend the content of the file.
From the command line:
```
cd merapar-challenge
vi secret //change the content of the file
aws s3 cp secret s3://merapar-challenge --acl public-read
```

### Uninstalling, destroying the environment:

To Uninstall and destroy or the resources, network components in AWS, please use the following command:
```
cd merapar-challenge
terraform destroy
```
## Future Upgrades, Modifications
In this challenge I tried to provide a simple solution for both the environment and handling the data. In a normal scenario the infrastructure can be improved by an Application Load Balancer and the instance can be placed to an Auto Scaling group to handle increased or reduced traffic. The string data also can be stored in ElastiCache, a Database or Lambda.
Security can be improved by more granular access control or restricting resources to IAM roles. 
