# Generate Outputs
output "web_instance_ip" {
  value = aws_instance.web.public_ip
}

output "elastic_ip" {
  value = aws_eip.vjeip.*.public_ip
}