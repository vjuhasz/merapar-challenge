variable "aws_region" {}

variable "instance_type" {}

variable "vpc_cidr" {}

variable "public_subnet_cidr" {}

variable "subnet_zone" {}

variable "bucket_name" {}
