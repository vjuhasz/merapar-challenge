# Create VPC
resource "aws_vpc" "vj-vpc" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "vj-vpc"

  }
}

#Create Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vj-vpc.id

  tags = {
    Name = "vpc_igw"
  }
}

# Create Public Subnet
resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.vj-vpc.id
  cidr_block              = var.public_subnet_cidr
  map_public_ip_on_launch = true
  availability_zone       = var.subnet_zone

  tags = {
    Name = "public-subnet"
  }

}

# Create Route Table
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vj-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "public_rt"
  }
}

# Assign Route Table to Subnet
resource "aws_route_table_association" "public_rt_asso" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.public_rt.id

}

# Create Elastic IP
resource "aws_eip" "vjeip" {
  vpc = true
}

# Assign Elastic IP to Ec2 Instance
resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.web.id
  allocation_id = aws_eip.vjeip.id
}