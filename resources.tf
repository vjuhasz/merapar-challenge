# Create S3 Bucket for storing variable
resource "aws_s3_bucket" "b" {
  bucket = var.bucket_name


  tags = {
    Name        = var.bucket_name
    Environment = "Dev"
  }
}

# Change Bucket permission to Public read
resource "aws_s3_bucket_acl" "set_bucket_acl" {
  bucket = aws_s3_bucket.b.id
  acl    = "public-read"
}



# Upload an object
resource "aws_s3_object" "this" {
  bucket = aws_s3_bucket.b.id
  key    = "secret"
  source = "secret"
  acl    = "public-read"


}

# Create Ec2 web instance
resource "aws_instance" "web" {
  ami             = data.aws_ami.amzlinux.id
  instance_type   = var.instance_type
  key_name        = "terraform-key"
  subnet_id       = aws_subnet.public_subnet.id
  security_groups = [aws_security_group.sg.id]
  user_data       = <<EOF
  #! /bin/bash
  sudo yum update -y
  sudo yum install -y httpd
  sudo systemctl enable httpd
  sudo service httpd start
  sudo yum install amazon-linux-extras -y
  sudo amazon-linux-extras enable php7.4 
  sudo yum clean metadata 
  sudo yum install php php-common php-pear -y
  sudo yum install php-{cgi,curl,mbstring,gd,mysqlnd,gettext,json,xml,fpm,intl,zip}  
  echo "<h1>The saved string is  
  <?php \$string = file_get_contents('https://${var.bucket_name}.s3.${var.aws_region}.amazonaws.com/secret');
  echo \$string ?>
  </h1> " | sudo tee /var/www/html/index.php
  sudo systemctl restart httpd
  EOF


  tags = {
    "Name" = "web"
  }
}
